import { encrypt, decrypt } from ".";
import { AESKeyGen, KeyPairGen } from "./keyderive";

(async () => {

    let key1 = await KeyPairGen();
    let key2 = await KeyPairGen();
    const stage1 = await AESKeyGen(key1.publicKey, key2)
    
    key1 = await KeyPairGen();
    key2 = await KeyPairGen();
    const stage2 = await AESKeyGen(key1.publicKey, key2)
    
    key1 = await KeyPairGen();
    key2 = await KeyPairGen();
    const stage3 = await AESKeyGen(key1.publicKey, key2)
    
    const ciphertext = await encrypt("SUPER SECRET TEXT", stage1, stage2, stage3);
    
    console.log(ciphertext);
    
    console.log(await decrypt(ciphertext, stage1, stage2, stage3))
})();


