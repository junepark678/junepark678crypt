export async function AESKeyGen(publicKey: CryptoKey, myPrivateKey: CryptoKeyPair){
    const crypt = crypto.subtle

    return await crypt.deriveKey(
        {
            name: 'ECDH',
            public: publicKey,
        },
        myPrivateKey.privateKey,
        {
            name: 'AES-GCM',
            length: 256
        },
        true,
        ['encrypt', 'decrypt']
    )
}

export async function KeyPairGen() {
    return await crypto.subtle.generateKey({
        name: 'ECDH',
        namedCurve: 'P-256',
    },
    true,
    ["deriveKey"])
}